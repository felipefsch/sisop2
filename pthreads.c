#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define	TRUE 1
#define FALSE 0

pthread_mutex_t **INDEXES;
int **matrixA;
int **matrixB;
int **matrixC;

int LINES_A = 0;
int COLUMNS_A = 0;
int LINES_B = 0;
int COLUMNS_B = 0;
int ITERATION_LIMIT = 0;

void *threadMultiplicaElementos(void *parametro) {
    
    int i = 0;
    int linha = 0;
    int coluna  = 0;
    int resultado = 0;

    linha = *(int *)parametro / 10 ;    
    coluna = *(int *)parametro % 10;    //MOD

	printf("Linha:%d \n", linha);
	printf("Coluna:%d \n", coluna);
    for(i = 0; i <= ITERATION_LIMIT; i++) {    
                
        resultado += matrixA[linha][i] * matrixB[i][coluna];
    }
    printf ("Encontrei %d\n\n", resultado);
    //return resultado;
    
}

void loadMatrixes() {
    FILE *file;

    int buffer = 0;
    int i;
    int j;

    if (!(file = fopen("in1.txt", "r")))
    {
        printf("Error while opening the file!");
        exit(0);
    }

    while (!feof(file)) {
        fscanf(file, "LINHAS = %d\n", &LINES_A);
        fscanf(file, "COLUNAS = %d\n", &COLUMNS_A);
        
        //documentar isto
        if(ITERATION_LIMIT == 0)    
		ITERATION_LIMIT = LINES_A;

        matrixA = (int**) malloc(LINES_A * sizeof (int));

        for (i = 0; i < LINES_A; i++) {
            matrixA[i] = (int*) malloc(COLUMNS_A * sizeof(int));
            for (j = 0; j < COLUMNS_A; j++) {
                fscanf(file, "%d", &buffer);
                matrixA[i][j] = buffer;
            }
            fscanf(file, "\n");
        }
    }
	fclose(file);
    if (!(file = fopen("in2.txt", "r")))
    {
        printf("Error while opening the file!");
        exit(0);
    }

    while (!feof(file)) {
        fscanf(file, "LINHAS = %d\n", &LINES_B);
        fscanf(file, "COLUNAS = %d\n", &COLUMNS_B);
        
        matrixB = (int**) malloc(LINES_B * sizeof (int));

        for (i = 0; i < LINES_B; i++) {
            matrixB[i] = (int*) malloc(COLUMNS_B * sizeof(int));
            for (j = 0; j < COLUMNS_B; j++) {
                fscanf(file, "%d", &buffer);
                matrixB[i][j] = buffer;
            }
            fscanf(file, "\n");
        }
    }
    fclose(file);

    matrixC = (int**) malloc(LINES_A * sizeof (int));
    INDEXES = (pthread_mutex_t**) malloc(LINES_A * sizeof (pthread_mutex_t));

    for (i = 0; i < LINES_A; i++) {
        matrixC[i] = (int*) malloc(COLUMNS_B * sizeof(int));
        INDEXES[i] = (pthread_mutex_t*) malloc(COLUMNS_B * sizeof(pthread_mutex_t));
        for (j = 0; j < COLUMNS_B; j++) {
            matrixC[i][j] = i*10 + j;
            INDEXES[i][j] = PTHREAD_MUTEX_INITIALIZER;
            //printf("MatrizC[%d][%d]: %d\n", i,j,matrixC[i][j]);
        }
    }

}

int main( int argc, char *argv[]) {
    pthread_t *threadMatrix;
    int elementoACalcular = 00;
    int numeroThreads = atoi(argv[1]);

    FILE *file;
    int buffer = 0;
    int i;
    int j;
    int totalElementos = 0;

    loadMatrixes();

    totalElementos = LINES_A * COLUMNS_B;
    //printf("total: %d", totalElementos); 


    threadMatrix = (pthread_t*) malloc(numeroThreads*sizeof(pthread_t));
        for (i=0; i<numeroThreads; i++) {
	printf("Criou uma thread\n");
            pthread_create(&threadMatrix[i], NULL, threadMultiplicaElementos, &matrixC[1][1]);
        }

    pthread_join(threadMatrix[0], NULL);

    
    //printf("acabando\n");
    pthread_exit(0);
}

