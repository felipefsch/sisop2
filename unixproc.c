#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> /* for pid_t */
#include <sys/wait.h> /* for wait */
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <errno.h>

//Semaphores
sem_t *sem_mult;
sem_t *sem_rest_mult;

//Shared matrices
int **matrixA;
int **matrixB;

//Global variables
int LINES_A = 0;
int COLUMNS_A = 0;
int LINES_B = 0;
int COLUMNS_B = 0;

//Shared variables id's
int COLUMNS_B_ID;
int LINES_A_ID;
int REM_MULT_ID;
int RESULT_MATRIX_ID;

//Constraints for each thread
int MAX_THREADS = 0;
int NUM_THREADS = 0;
int THREAD_MULT = 0;
int REM_MULT = 0;

void loadMatrices() {
	FILE *file;

	int buffer = 0;
	int i;
	int j;

	//Creation of first matrix
	if (!(file = fopen("in1.txt", "r"))) {
		printf("Error while opening the file!");
		exit(0);
	}

	while (!feof(file)) {
		fscanf(file, "LINHAS = %d\n", &LINES_A);
		fscanf(file, "COLUNAS = %d\n", &COLUMNS_A);

		matrixA = (int**) malloc(LINES_A * sizeof (int));

		for (i = 0; i < LINES_A; i++) {
		    matrixA[i] = (int*) malloc(COLUMNS_A * sizeof(int));
		    for (j = 0; j < COLUMNS_A; j++) {
			fscanf(file, "%d", &buffer);
			matrixA[i][j] = buffer;
		    }
		    fscanf(file, "\n");
		}
	}

	//Creation of second matrix
	if (!(file = fopen("in2.txt", "r"))) {
		printf("Error while opening the file!");
		exit(0);
	}

	while (!feof(file)) {
		fscanf(file, "LINHAS = %d\n", &LINES_B);
		fscanf(file, "COLUNAS = %d\n", &COLUMNS_B);

		matrixB = (int**) malloc(LINES_B * sizeof (int));

		for (i = 0; i < LINES_B; i++) {
		    matrixB[i] = (int*) malloc(COLUMNS_B * sizeof(int));
		    for (j = 0; j < COLUMNS_B; j++) {
			fscanf(file, "%d", &buffer);
			matrixB[i][j] = buffer;
		    }
		    fscanf(file, "\n");
		}
	}
	fclose(file);
}

int matrixMultiplication(int line, int column) {
	int i;
	int result = 0;

	for (i = 0; i < COLUMNS_A; i++) {
		result += matrixA[line][i] * matrixB[i][column];
	}

	return result;
}

void initConstraints() {
	MAX_THREADS = LINES_A * COLUMNS_B;

	//Defining how many multiplications per thread
	THREAD_MULT = (MAX_THREADS - (MAX_THREADS % NUM_THREADS)) / NUM_THREADS; 
	REM_MULT = MAX_THREADS % NUM_THREADS;
}

void outputResult() {
	FILE *file;

	if (!(file = fopen("out.txt", "w"))) {
		printf("Error while creating output file!\n");
		exit(0);
	}

	int *result_matrix = shmat(RESULT_MATRIX_ID, NULL, 0);

	fprintf(file, "LINHAS = %d\n", LINES_A);
	fprintf(file, "COLUNAS = %d\n", COLUMNS_B);

	int i;
	int j;
	for (i = 0; i < LINES_A; i++) {
		for (j = 0; j < COLUMNS_B; j++) {
			fprintf(file, "%d ", result_matrix[(i * COLUMNS_B) + j]);		
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

void printResultMatrix() {
	int *result_matrix = shmat(RESULT_MATRIX_ID, NULL, 0);

	int i;
	int j;

	for (i = 0; i < LINES_A; i++) {
		for (j = 0; j < COLUMNS_B; j++) {
			printf("%d ", result_matrix[(i * COLUMNS_B) + j]);		
		}
		printf("\n");
	}
}

int main(int argc, char *argv[]) {

	if (argc != 2) {
		printf("Missing parameters in %s, you must define how many processes you want to execute", argv[0]);
	}
	else {
		NUM_THREADS = atol(argv[1]);
		printf("Executing multiplication with %d UNIX processes...\n", NUM_THREADS);		
	}

    	loadMatrices();

	initConstraints();

	//Alocation of shared memory segment	
	LINES_A_ID = shmget(IPC_PRIVATE, sizeof(int), 0600);
	COLUMNS_B_ID = shmget(IPC_PRIVATE, sizeof(int), 0600);
	REM_MULT_ID = shmget(IPC_PRIVATE, sizeof(int), 0600);
	RESULT_MATRIX_ID = shmget(IPC_PRIVATE, LINES_A * COLUMNS_B * sizeof(int), 0600);

	//Attachment of shared memory segments
	int *LINES_A_SHR = shmat(LINES_A_ID, NULL, 0);
	int *COLUMNS_B_SHR = shmat(COLUMNS_B_ID, NULL, 0);
	int *REM_MULT_SHR = shmat(REM_MULT_ID, NULL, 0);
	int *RESULT_MATRIX = shmat(RESULT_MATRIX_ID, NULL, 0);

	//Atribute initial values of shared memory segments
	*LINES_A_SHR = LINES_A - 1;
	*COLUMNS_B_SHR = COLUMNS_B - 1;
	*REM_MULT_SHR = REM_MULT;

	//Open named semaphores
        if ((sem_mult = sem_open("/sem_mult", O_CREAT, 0644, 1)) == SEM_FAILED) {
                perror("sem_mult initilization");
                exit(1);
        }
        if ((sem_rest_mult = sem_open("/sem_rest_mult", O_CREAT, 0644, 1)) == SEM_FAILED) {
                perror("sem_rest_mult initilization");
                exit(1);
        }
	
	//sem_post(sem_mult); //Uncomment this line if you have a deadlock (semaphore not closed)

	pid_t pid;
	int k;
	for (k = 0; k < NUM_THREADS; k++) {
		pid = fork();

		//Child here
		if (pid == 0) {

			for (THREAD_MULT; THREAD_MULT > 0; THREAD_MULT--){

				sem_wait(sem_mult);
				int line = *LINES_A_SHR;
				int column = *COLUMNS_B_SHR;
				*COLUMNS_B_SHR = *COLUMNS_B_SHR - 1;
				if (*COLUMNS_B_SHR < 0) {
					*COLUMNS_B_SHR = COLUMNS_B - 1;
					*LINES_A_SHR = *LINES_A_SHR - 1;
				}	
				if (*REM_MULT_SHR > 0) {
					*REM_MULT_SHR = *REM_MULT_SHR - 1;
					int line_rem = *LINES_A_SHR;
					int column_rem = *COLUMNS_B_SHR;
					*COLUMNS_B_SHR = *COLUMNS_B_SHR - 1;
					if (*COLUMNS_B_SHR < 0) {
						*COLUMNS_B_SHR = COLUMNS_B - 1;
						*LINES_A_SHR = *LINES_A_SHR - 1;
					}	
					int result_rem = matrixMultiplication(line_rem, column_rem);
					RESULT_MATRIX[(line_rem * COLUMNS_B) + (column_rem)] = result_rem;
					/*printf("REM! pid: %d execution: %d line: %d column: %d result: %d v_pos: %d \n", pid, THREAD_MULT, line_rem, column_rem, result_rem, (line_rem * COLUMNS_B) + (column_rem));*/
				}
				sem_post(sem_mult);
				int result = matrixMultiplication(line, column);
				RESULT_MATRIX[(line * COLUMNS_B) + column] = result;
				/*printf("pid: %d execution: %d line: %d column: %d result: %d v_pos: %d\n",
					pid, THREAD_MULT, line, column, result, (line * COLUMNS_B) + 						(column));*/
			}
			exit(0);
		}
	}
	waitpid(pid, 0, 0);
	
	//Close named semaphores
        if (sem_close(sem_mult) < 0) {
                perror("sem_mult close");
                exit(1);
        }
        if (sem_close(sem_rest_mult) < 0) {
                perror("sem_rest_mult close");
                exit(1);
        }

	//printResultMatrix();

	outputResult();

	printf("Multiplication done!\n");	

	return 0;
}
