/*
 * dining-philosophers-pthread-cond.c
 *
 * T2 SISOP2 - UFRGS
 * 
 * Felipe Schmidt <felipefsch@gmail.com>
 * Gabriel Visconti <gbvisconti@gmail.com>
 * Germano Andersson <germanoa@gmail.com>
 *
 * 2013-04-14
 *
 */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

#define	TRUE 1
#define	FALSE 0

#define	THINKING 0
#define HUNGRY 1
#define EATING 2

#define MAXPHILOSOPHERS 32768

#define SPEEDGAME 5

int *state;
pthread_cond_t *philosopher_can_test_again;
int total_philosophers;

pthread_mutex_t mutex;

//because -1%5 returns -1, not 4
int static inline __mod(x,y) {
    int res = x % y;
    if (res < 0)
        res+=y;
    return res;
} 

void static inline __print_table_view() {
    int i;
    for (i=0; i < total_philosophers; i++) {
        switch(state[i]) {
        case THINKING:
            printf("T ");
            break;
        case HUNGRY:
            printf("H ");
            break;
        case EATING:
            printf("E ");
            break;
        default:
            break;
        }
    }
    printf("\n");
}

static inline int __neighbor_left(int philosopher) { 
    return __mod((philosopher - 1),total_philosophers);
}

static inline int __neighbor_right(int philosopher) {
    return __mod((philosopher + 1),total_philosophers);
}

static inline void __change_state(int philosopher, int s) {
    state[philosopher] = s; __print_table_view();
}

void test(int philosopher) {
    if ( state[__neighbor_left(philosopher)] != EATING &&
         state[__neighbor_right(philosopher)] != EATING &&
         state[philosopher] == HUNGRY ) {
        __change_state(philosopher,EATING);
    }
}

void pickup(int philosopher) {
    pthread_mutex_lock(&mutex);
    __change_state(philosopher,HUNGRY);
    test(philosopher);
    while (state[philosopher] != EATING) {
        pthread_cond_wait(&philosopher_can_test_again[philosopher], &mutex);
        test(philosopher);
    }
    pthread_mutex_unlock(&mutex);
}

void putdown(int philosopher) {
    pthread_mutex_lock(&mutex);
    __change_state(philosopher,THINKING);
    pthread_mutex_unlock(&mutex);
    pthread_cond_signal(&philosopher_can_test_again[__neighbor_left(philosopher)]);
    pthread_cond_signal(&philosopher_can_test_again[__neighbor_right(philosopher)]);
}

void think(int philosopher) {
    sleep(rand()%SPEEDGAME);
}

void eat(int philosopher) {
    pickup(philosopher);
    sleep(rand()%SPEEDGAME);
    putdown(philosopher);
}

void *philosophize(void *arg) {
    int philosopher = (int) arg;
    while (TRUE) {
        think(philosopher);
        eat(philosopher);
    }
}

int main( int argc, char *argv[]) {

    if (argc < 2) { fprintf(stderr, "ERROR, use %s N\n", argv[0]); exit(1); }

    if (atoi(argv[1]) > MAXPHILOSOPHERS) { exit(1); }
    total_philosophers = atoi(argv[1]);

    pthread_t *threadPhilosopher;

    if ( (state = (int*) malloc(total_philosophers * sizeof(int))) == NULL ) { exit(1); }
    if ( (philosopher_can_test_again = (pthread_cond_t*) malloc(total_philosophers * sizeof(pthread_cond_t))) == NULL ) { exit(1); }
    if ( (threadPhilosopher = (pthread_t*) malloc(total_philosophers * sizeof(pthread_t))) == NULL ) { exit(1); };

    int i;
    for (i=0; i < total_philosophers; i++) {
        pthread_cond_init(&philosopher_can_test_again[i], NULL);
    }
    for (i=0; i < total_philosophers; i++) {
        pthread_create(&threadPhilosopher[i], NULL, (void *)philosophize,(void *) i);
    }
    for (i=0; i < total_philosophers; i++) {
        pthread_join(threadPhilosopher[i], NULL);
    }
}
