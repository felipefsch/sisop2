To compile this project you need just to run the following command line:

gcc -o pthreads pthreads.c -lpthread

Afterwards, you just need to run the program with the following cmd line: ./pthreads
Note that you will need te files 'in1.txt' and 'in2.txt' that represents the matrices you want to
multiplicate in the same folder as your exec file.

To the unixproc you just need to:

gcc unixproc.c -o unixproc -pthread 
