/*
 * dining-philosophers-pthread-sem.c
 *
 * T2 SISOP2 - UFRGS
 * 
 * Felipe Schmidt <felipefsch@gmail.com>
 * Gabriel Visconti <gbvisconti@gmail.com>
 * Germano Andersson <germanoa@gmail.com>
 *
 * 2013-04-14
 *
 */

#include <semaphore.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

#define	TRUE 1
#define	FALSE 0

#define	THINKING 0
#define HUNGRY 1
#define EATING 2

#define MAXPHILOSOPHERS 32768

#define SPEEDGAME 5

int *state;
sem_t *forks;
sem_t changing_state;
int total_philosophers;

//because -1%5 returns -1, not 4
int static inline __mod(x,y) {
    int res = x % y;
    if (res < 0)
        res+=y;
    return res;
} 

void static inline __print_table_view() {
    int i;
    for (i=0; i < total_philosophers; i++) {
        switch(state[i]) {
        case THINKING:
            printf("T ");
            break;
        case HUNGRY:
            printf("H ");
            break;
        case EATING:
            printf("E ");
            break;
        default:
            break;
        }
    }
    printf("\n");
}

void static inline  __change_state(int philosopher, int s) {
    sem_wait(&changing_state);
    state[philosopher] = s; __print_table_view();
    sem_post(&changing_state);
}

void think(int philosopher) {
    sleep(rand()%SPEEDGAME);
}

void eat(int philosopher) {
    __change_state(philosopher,HUNGRY);
    if (philosopher % 2 == 0) {
        sem_wait(&forks[philosopher]);
        sem_wait(&forks[__mod(philosopher+1,total_philosophers)]);
    }
    else {
        sem_wait(&forks[__mod(philosopher+1,total_philosophers)]);
        sem_wait(&forks[philosopher]);
    }    
    __change_state(philosopher,EATING);
    sleep(rand()%SPEEDGAME);
    sem_post(&forks[philosopher]);
    sem_post(&forks[__mod(philosopher+1,total_philosophers)]);
    __change_state(philosopher,THINKING);
}

void *philosophize(void *arg) {
    int philosopher = (int) arg;
    while (TRUE) {
        think(philosopher);
        eat(philosopher);
    }
}

int main( int argc, char *argv[]) {

    if (argc < 2) { fprintf(stderr, "ERROR, use %s N\n", argv[0]); exit(1); }

    if (atoi(argv[1]) > MAXPHILOSOPHERS) { exit(1); }
    total_philosophers = atoi(argv[1]);

    pthread_t *threadPhilosopher;

    if ( (state = (int*) malloc(total_philosophers * sizeof(int))) == NULL ) { exit(1); }
    if ( (forks = (sem_t*) malloc(total_philosophers * sizeof(sem_t))) == NULL ) { exit(1); }
    if ( (threadPhilosopher = (pthread_t*) malloc(total_philosophers * sizeof(pthread_t))) == NULL ) { exit(1); };


    sem_init(&changing_state, 0, 1);
    int i;
    for (i=0; i < total_philosophers; i++) {
        sem_init(&forks[i], 0, 1);
    }
    for (i=0; i < total_philosophers; i++) {
        pthread_create(&threadPhilosopher[i], NULL, (void *)philosophize,(void *) i);
    }
    for (i=0; i < total_philosophers; i++) {
        pthread_join(threadPhilosopher[i], NULL);
    }
}
